import { Response } from "express";
import * as client from "prom-client";
import { Controller, Get, Res } from "routing-controllers";
import { OpenAPI } from "routing-controllers-openapi";

@Controller()
export class IndexController {
	@Get("/")
	index() {
		return "Welcome!";
	}

	@Get("/metrics")
	@OpenAPI({ summary: "Grafana metrics" })
	async getMetrics(@Res() res: Response) {
		res.set("Content-Type", client.register.contentType);
		return await client.register.metrics();
	}
}
