process.env["NODE_CONFIG_DIR"] = __dirname + "/configs";

import { validationMetadatasToSchemas } from "class-validator-jsonschema";
import * as client from "prom-client";
import compression from "compression";
import cookieParser from "cookie-parser";
import config from "config";
import express from "express";
import helmet from "helmet";
import hpp from "hpp";
import morgan from "morgan";
import * as Sentry from "@sentry/node";
import * as Tracing from "@sentry/tracing";
import { useExpressServer, getMetadataArgsStorage } from "routing-controllers";
import { routingControllersToSpec } from "routing-controllers-openapi";
import swaggerUi from "swagger-ui-express";
import errorMiddleware from "@middlewares/error.middleware";
import { logger, stream } from "@utils/logger";

class App {
	public app: express.Application;
	public port: string | number;
	public env: string;

	constructor(Controllers: Function[]) {
		this.configureGrafana();
		this.app = express();
		this.port = process.env.PORT || 3000;
		this.env = process.env.NODE_ENV || "development";

		this.initializeSentry(this.app);
		this.initializeSentryHanlders();
		this.initializeMiddlewares();
		this.initializeRoutes(Controllers);
		this.initializeSwagger(Controllers);
		this.initializeSentryErrorHandling();
		this.initializeErrorHandling();
	}

	public listen() {
		this.app.listen(this.port, () => {
			logger.info(`=================================`);
			logger.info(`======= ENV: ${this.env} =======`);
			logger.info(`🚀 App listening on the port ${this.port}`);
			logger.info(`=================================`);
		});
	}

	public getServer() {
		return this.app;
	}

	public configureGrafana() {
		return client.collectDefaultMetrics();
	}

	private initializeMiddlewares() {
		this.app.use(morgan(config.get("log.format"), { stream }));
		this.app.use(hpp());
		this.app.use(helmet());
		this.app.use(compression());
		this.app.use(express.json());
		this.app.use(express.urlencoded({ extended: true }));
		this.app.use(cookieParser());
	}

	private initializeRoutes(controllers: Function[]) {
		useExpressServer(this.app, {
			cors: {
				origin: config.get("cors.origin"),
				credentials: config.get("cors.credentials"),
			},
			controllers: controllers,
			defaultErrorHandler: false,
		});
	}

	private initializeSwagger(controllers: Function[]) {
		const { defaultMetadataStorage } = require("class-transformer/cjs/storage");

		const schemas = validationMetadatasToSchemas({
			classTransformerMetadataStorage: defaultMetadataStorage,
			refPointerPrefix: "#/components/schemas/",
		});

		const routingControllersOptions = {
			controllers: controllers,
		};

		const storage = getMetadataArgsStorage();
		const spec = routingControllersToSpec(storage, routingControllersOptions, {
			components: {
				schemas,
				securitySchemes: {
					basicAuth: {
						scheme: "basic",
						type: "http",
					},
				},
			},
			info: {
				description: "Generated with `routing-controllers-openapi`",
				title: "A sample API",
				version: "1.0.0",
			},
		});

		this.app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(spec));
	}

	private initializeSentry(app: express.Application) {
		Sentry.init({
			dsn: process.env.SENTRY_URL,
			integrations: [
				// enable HTTP calls tracing
				new Sentry.Integrations.Http({ tracing: true }),
				// enable Express.js middleware tracing
				new Tracing.Integrations.Express({ app }),
			],

			// Set tracesSampleRate to 1.0 to capture 100%
			// of transactions for performance monitoring.
			// We recommend adjusting this value in production
			tracesSampleRate: 1.0,
		});
	}

	private initializeSentryHanlders() {
		this.app.use(Sentry.Handlers.requestHandler());
		this.app.use(Sentry.Handlers.tracingHandler());
	}

	private initializeSentryErrorHandling() {
		this.app.use(
			Sentry.Handlers.errorHandler({
				shouldHandleError(error) {
					// Capture all 404 and 500 errors
					if (error.status > 399) {
						return true;
					}
					return false;
				},
			})
		);
	}

	private initializeErrorHandling() {
		this.app.use(errorMiddleware);
	}
}

export default App;
